module gitea.com/macaron/csrf

go 1.11

require (
	gitea.com/macaron/macaron v1.5.1-0.20201027213641-0db5d4584804
	gitea.com/macaron/session v0.0.0-20190821211443-122c47c5f705
	github.com/gopherjs/gopherjs v0.0.0-20190430165422-3e4dfb77656c // indirect
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337
	github.com/unknwon/com v1.0.1
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897 // indirect
	gopkg.in/ini.v1 v1.62.0 // indirect
)
